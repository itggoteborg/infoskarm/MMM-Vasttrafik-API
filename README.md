# MMM-vasttrafik-API
This is a fork of [ITG-Infoskarm-API](https://github.com/joel-eriksson/ITG-Infoskarm-API).

## License
MIT License

Copyright &copy; 2017-2018 Joel Ericsson <joel.eriksson@protonmail.com>
