/**
 * Namespace for the config.json file
 * @since 0.0.1
 * @namespace config
 */
namespace Config {
	export type FirebaseConfig = {
		databaseURL: string
	}

	export type VasttrafikStop = {
		id: string
		slug: string
	}

	export type VasttrafikConfig = {
		accessTokenUrl: string
		consumerKey: string
		consumerSecret: string
		updateInterval: number
		timeSpan: number
		stops: VasttrafikStop[]
	}
	
	export type Config = {
		firebase: FirebaseConfig
		vasttrafik: VasttrafikConfig
	}
}

export default Config
