import * as path from "path"

import chalk from "chalk"
import * as admin from "firebase-admin"
import * as urlSlug from "url-slug"
import * as moment from "moment-timezone"

import Config from "./config"
import Auth from "./lib/auth"
import apirequester from "./lib/apirequester/apirequester"
import vasttrafik from "./lib/vasttrafik/vasttrafik"

const cProperty = chalk.cyan

const cError = (error: string | Error): string => {
	if (error instanceof Error) {
		return chalk.red(error.message)
	}

	return chalk.red(error)
}

const cTimestamp = (timestamp: number | string | Date | moment.Moment = Date.now(), outputFormat: string = "HH:mm:ss"): string => {
	const momentStamp: string = moment(timestamp).tz("Europe/Stockholm").format(outputFormat)
	
	return `[${chalk.dim(momentStamp)}]`
}

const cLibName = (lib: string): string => {
	return `'${chalk.magenta(lib)}'`
}

/**
 * The object obtained from the config file
 * @constant {config.Config}
 */
const config: Config.Config = require(path.resolve("config", "config.json"))

admin.initializeApp({
	credential: admin.credential.cert(path.resolve("config", "firebaseServiceAccount.json")),
	databaseURL: config.firebase.databaseURL,
})

/**
 * Firebase database root reference
 * @constant {admin.database.Database}
 */
const rootRef: admin.database.Reference = admin.database().ref()

/**
 * An instance of [[Auth]] to get an access token for Västtrafik's APIs
 * @const {Auth}
 */
const vasttrafikAuth: Auth = new Auth(config.vasttrafik.accessTokenUrl, config.vasttrafik.consumerKey, config.vasttrafik.consumerSecret)

/**
 * An instance of [[apirequester.APIRequester]] for use with [[vasttrafik.API]]
 * @constant {apirequester.APIRequester}
 */
const vasttrafikAPIRequester: apirequester.APIRequester = new apirequester.APIRequester(40, 60000)

/**
 * An instance of [[vasttrafik.API]] to handle requests to Västtrafik's APIs
 * @constant {vasttrafik.API}
 */
const vasttrafikAPI: vasttrafik.API = new vasttrafik.API(vasttrafikAPIRequester, vasttrafikAuth)

const getDepartures = () => {
	/**
	 * An array of promises to fetch Västtrafik stops
	 * @constant {Promise[]}
	 */
	const stopFetches = []
	config.vasttrafik.stops.forEach((stop) => {
		stopFetches.push(vasttrafikAPI.getDepartures(stop.id, moment(), config.vasttrafik.timeSpan))
	})

	Promise.all(stopFetches).then((results) => {
		const vasttrafikRef: admin.database.Reference = rootRef.child("vasttrafik")

		results.forEach((result) => {
			if (result && result.stop.name) {
				const parsedDeparturesKey: string = urlSlug(result.stop.name)
				
				vasttrafikRef.child("stopsLookup").child(result.stop.id).set(parsedDeparturesKey).catch((error) => {
					console.error(`${cTimestamp()} ${cLibName("vasttrafik")} Could not add ${cProperty(result.stop.id)}: ${cProperty(parsedDeparturesKey)} to Firebase.`)
				})

				vasttrafikRef.child("departures").child(parsedDeparturesKey).set(result).then(() => {
					console.log(`${cTimestamp()} ${cLibName("vasttrafik")} Wrote stop ${cProperty(result.stop.shortName)} to Firebase.`)
				}).catch((error) => {
					console.error(`${cTimestamp()} ${cLibName("vasttrafik")} ${cError(error)}`)
				})
			} else if (result.stop.id) {
				vasttrafikRef.child("stopsLookup").child(result.stop.id).once("value", (stopLookupSnap) => {
					let stopSlug = stopLookupSnap.val()

					if (!stopSlug) {
						({ slug: stopSlug } = config.vasttrafik.stops.find((stopToFetch) => {
							return result.stop.id === stopToFetch.id
						}))

						if (!stopSlug) {
							return
						}
					}

					vasttrafikRef.child("departures").child(stopSlug).child("stop").once("value", (stopSnap) => {
						vasttrafikRef.child("departures").child(stopSlug).child("departures").set(null).then(() => {
							console.log(`${cTimestamp()} ${cLibName("vasttrafik")} Wrote stop ${cProperty(stopSnap.val().shortName)} to Firebase.`)
						}).catch((error) => {
							console.error(`${cTimestamp()} ${cLibName("vasttrafik")} ${cError(error)}`)
						})
					})
				})
			}
		})
	}).catch((error) => {
		console.error(`${cTimestamp()} ${cLibName("vasttrafik")} ${cError(error)}`)
	})
}

setInterval(() => {
	let seconds: number = parseInt(moment().format("s"))

	if (seconds % Math.max(10, config.vasttrafik.updateInterval) === 0) {
		getDepartures()
	}
}, 1000)
